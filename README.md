# homeassistant-magiwand

[![PyPI version](https://badge.fury.io/py/homeassistant-magiwand.svg)](https://badge.fury.io/py/homeassistant-magiwand)

This code can be used to connect MagiQuest IR wands to an instance of HomeAssistant.  This extends a related project of mine, pywand [[source](https://gitlab.com/rveach/wand)] [[PyPI](https://pypi.org/project/pywand/)]

This project ships with a script that will monitor a lirc device, and trigger HomeAssistant Automations via web api call. Currently, the scope is pretty narrow, but I hope to expand it over time to meet my needs.

## Installation, Setup, and Config

#### tldr:

- Enable your infrared device
- Install python3
- `pip install homeassistant-magiwand`
- Enable a Ling-Lived Access Token in HomeAssistant
- Enable api and add an Automation to run in HomeAssistant
- Edit `/etc/wand_config.ini`
- Run `ha_wand_service.py`

#### More Details

- [pywand: Installation and Pre-Reqs](https://gitlab.com/rveach/wand/-/wikis/Installation-and-Pre-Reqs)
- [pywand: Configuration](https://gitlab.com/rveach/wand/-/wikis/pywand-Configuration)
- Enable the REST API for HomeAssistant: [HomeAssistant: REST API](https://developers.home-assistant.io/docs/en/external_api_rest.html)
- Create an Automation for our sensor. This is what will be triggered when the wand is waved. [HomeAssistant: Automation](https://www.home-assistant.io/integrations/automation/)
- Create a Long-Lived Access Token within your HomeAssistant Instance.
- Continue configuring the wand configuration file with the HOMEASSISTANT section. See [example_wand_config.ini](https://gitlab.com/rveach/homeassistant-magiwand/blob/master/example_wand_config.ini) for more information.
- Execute the service by running `ha_wand_service.py`

## Running the service

Just make sure the config file is right, see previous sections

#### Usage

```
ha_wand_service.py -h 
usage: ha_wand_service.py [-h] [--loglevel LOGLEVEL]

optional arguments:
  -h, --help           show this help message and exit
  --loglevel LOGLEVEL  Loglevel: CRITICAL, ERROR, WARNING (default), INFO,
                       DEBUG
```